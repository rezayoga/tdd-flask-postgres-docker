# src/__init__.py

import logging
import os
import uuid

import bugsnag
import flask_login as login
from bugsnag.flask import handle_exceptions
from bugsnag.handlers import BugsnagHandler
from flask import Flask, redirect, render_template, request, session, url_for
from flask_admin import Admin, AdminIndexView, expose, helpers
from flask_migrate import Migrate
from werkzeug.middleware.proxy_fix import ProxyFix  # new
from werkzeug.security import check_password_hash
from wtforms import fields, form, validators

from src.api.models import Book, Channel, User, UserChannel
from src.extensions import db

migrate = Migrate()
bugsnag.configure(
    api_key="5ea120a6da7a09968c4b48c630560044",
    project_root=os.getcwd(),
)


def create_app(script_info=None):

    # instantiate the app
    app = Flask(__name__)
    # app.config["FLASK_ADMIN_SWATCH"] = "simplex"
    app.wsgi_app = ProxyFix(app.wsgi_app, x_for=1, x_proto=1, x_host=1, x_port=1)  # new

    # set config
    app_settings = os.getenv("APP_SETTINGS")
    app.config.from_object(app_settings)

    # bug snag
    handle_exceptions(app)

    # app.secret_key = "my_precious"
    # app.config["SESSION_TYPE"] = "filesystem"

    # set up extensions
    register_extensions(app)

    # if os.getenv("FLASK_ENV") == "development":

    login_manager = login.LoginManager()
    login_manager.init_app(app)

    # Define login and registration forms (for flask-login)
    class LoginForm(form.Form):
        username = fields.StringField(validators=[validators.DataRequired()])
        password = fields.PasswordField(validators=[validators.DataRequired()])

        def validate_login(self, field):
            user = self.get_user()

            if user is None:
                raise validators.ValidationError("Invalid user")

            # we're comparing the plaintext pw with the the hash from the db
            if not check_password_hash(user.password, self.password.data):
                # to compare plain text passwords use
                # if user.password != self.password.data:
                raise validators.ValidationError("Invalid password")

        def get_user(self):
            user = User.query.filter_by(username=self.username.data).first()

            if user and user.check_password(self.password.data):
                return user
            else:
                return None

    class RegistrationForm(form.Form):
        username = fields.StringField(validators=[validators.DataRequired()])
        email = fields.StringField()
        password = fields.PasswordField(validators=[validators.DataRequired()])

        def validate_login(self, field):
            if (
                db.session.query(User).filter_by(username=self.username.data).count()
                > 0
            ):
                raise validators.ValidationError("Duplicate username")

    # Create customized index view class that handles login & registration
    class MyAdminIndexView(AdminIndexView):
        @expose("/")
        def index(self):
            if not login.current_user.is_authenticated:
                return redirect(url_for(".login_view"))
            return super(MyAdminIndexView, self).index()

        @expose("/login/", methods=("GET", "POST"))
        def login_view(self):
            # handle user login
            form = LoginForm(request.form)
            if helpers.validate_form_on_submit(form):
                user = form.get_user()
                if user is not None:
                    login.login_user(user)
                else:
                    # raise validators.ValidationError('Invalid account')
                    session["error"] = "Invalid account"
                    redirect(url_for(".index"))

            if login.current_user.is_authenticated:
                return redirect(url_for(".index"))
            link = (
                "<p>Don't have an account? <a href=\""
                + url_for(".register_view")
                + '">Click here to register.</a></p>'
            )
            self._template_args["form"] = form
            self._template_args["link"] = link
            return super(MyAdminIndexView, self).index()

        @expose("/register/", methods=("GET", "POST"))
        def register_view(self):
            form = RegistrationForm(request.form)
            if helpers.validate_form_on_submit(form):
                # we hash the users password to avoid saving it as plaintext in the db,
                # remove to use plain text:

                user = User(
                    public_id=str(uuid.uuid4()),
                    username=form.username.data,
                    email=form.email.data,
                    password=form.password.data,
                )
                # form.populate_obj(user)
                db.session.add(user)
                db.session.commit()

                login.login_user(user)
                return redirect(url_for(".index"))
            link = (
                '<p>Already have an account? <a href="'
                + url_for(".login_view")
                + '">Click here to log in.</a></p>'
            )
            self._template_args["form"] = form
            self._template_args["link"] = link
            return super(MyAdminIndexView, self).index()

        @expose("/logout/")
        def logout_view(self):
            login.logout_user()
            return redirect(url_for(".index"))

    # if os.getenv("FLASK_ENV") == "development":

    admin = Admin(
        app,
        "Admin Panel",
        index_view=MyAdminIndexView(),
        base_template="my_master.html",
        template_mode="bootstrap4",
    )

    from src.api.users.admin import UsersAdminView  # new

    admin.add_view(UsersAdminView(User, db.session))  # updated
    from src.api.books.admin import BooksAdminView  # new

    admin.add_view(BooksAdminView(Book, db.session))  # updated
    from src.api.channels.admin import ChannelsAdminView  # new

    admin.add_view(ChannelsAdminView(Channel, db.session))  # updated
    from src.api.users_channels.admin import UsersChannelsAdminView  # new

    admin.add_view(UsersChannelsAdminView(UserChannel, db.session))  # updated

    @login_manager.user_loader
    def load_user(user_id):
        return User.query.get(int(user_id))

    # Flask views
    @app.route("/")
    def index():
        return render_template("index.html")

    # register api
    from src.api import api

    api.init_app(app)

    # Configure logging
    configure_logging(app)

    # Register error handlers
    register_error_handlers(app)

    return app


def register_extensions(app):

    db.init_app(app)

    # flask-migrate initialization
    migrate.init_app(app, db)

    # shell context for flask cli
    @app.shell_context_processor
    def ctx():
        return {"app": app, "db": db}


def register_error_handlers(app):

    # 400 - Bad Request
    @app.errorhandler(400)
    def bad_request(e):
        return render_template("400.html"), 400

    # 403 - Forbidden
    @app.errorhandler(403)
    def forbidden(e):
        return render_template("403.html"), 403

    # 404 - Page Not Found
    @app.errorhandler(404)
    def page_not_found(e):
        return render_template("404.html"), 404

    # 405 - Method Not Allowed
    @app.errorhandler(405)
    def method_not_allowed(e):
        return render_template("405.html"), 405

    # 500 - Internal Server Error
    @app.errorhandler(500)
    def server_error(e):
        return render_template("500.html"), 500


def configure_logging(app):

    # Deactivate the default flask logger so that log messages don't get duplicated
    # app.logger.removeHandler(default_handler)

    # Create a file handler object
    # file_handler = RotatingFileHandler('flaskapp.log', maxBytes=16384, backupCount=20)

    # Set the logging level of the file handler object so that it logs INFO and up
    # file_handler.setLevel(logging.INFO)

    # Create a file formatter object
    # file_formatter = logging.Formatter('%(asctime)s %(levelname)s: %(message)s [in %(filename)s: %(lineno)d]')

    # Apply the file formatter object to the file handler object
    # file_handler.setFormatter(file_formatter)

    # Add file handler object to the logger
    # app.logger.addHandler(file_handler)

    logger = logging.getLogger("test.logger")

    handler = BugsnagHandler()
    # send only ERROR-level logs and above
    # handler.setLevel(logging.ERROR)
    # logger.addHandler(handler)

    logger.addFilter(handler.leave_breadcrumbs)
