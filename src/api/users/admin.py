# src/api/users/admin.py

from flask_admin.contrib.sqla import ModelView
from flask_login import current_user


class UsersAdminView(ModelView):
    def is_accessible(self):
        return current_user.is_authenticated

    column_list = (
        "public_id",
        "username",
        "email",
        "password",
        "books",
        "following",
        "created_date",
    )
    column_searchable_list = (
        "username",
        "email",
    )
    column_editable_list = (
        "username",
        "email",
    )
    column_filters = (
        "username",
        "email",
        "books",
        "following",
    )
    column_sortable_list = (
        "username",
        "email",
        "active",
        "created_date",
    )
    column_default_sort = ("created_date", True)
