# src/api/__init__.py

from flask_restx import Api

from src.api.books.views import books_namespace
from src.api.channels.views import channels_namespace
from src.api.ping import ping_namespace
from src.api.users.views import users_namespace
from src.api.users_channels.views import users_channels_namespace

api = Api(version="1.0", title="Test API", doc="/doc")  # updated

api.add_namespace(ping_namespace, path="/ping")
api.add_namespace(users_namespace, path="/users")
api.add_namespace(channels_namespace, path="/channels")
api.add_namespace(books_namespace, path="/books")
api.add_namespace(users_channels_namespace, path="/users_channels")
