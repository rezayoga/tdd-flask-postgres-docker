# src/api/books/views.py

from flask import request
from flask_restx import Namespace, Resource, fields  # updated

from src.api.books.crud import (  # isort:skip
    get_all_books,
    get_book_by_title,
    add_book,
    get_book_by_id,
    update_book,
    delete_book,
)

books_namespace = Namespace("books")  # new

# updated
book = books_namespace.model(
    "Book",
    {
        "id": fields.Integer(readOnly=True),
        "title": fields.String(required=True),
        "author": fields.String(required=True),
        "created_date": fields.DateTime,
    },
)


class BooksList(Resource):
    @books_namespace.marshal_with(book, as_list=True)  # updated
    def get(self):
        return get_all_books(), 200

    @books_namespace.expect(book, validate=True)  # updated
    def post(self):
        post_data = request.get_json()
        title = post_data.get("title")
        author = post_data.get("author")
        response_object = {}

        book = get_book_by_title(title)
        if book:
            response_object["message"] = "Sorry. That book already exists."
            return response_object, 400

        add_book(title, author)

        response_object["message"] = f"{title} was added!"
        return response_object, 201


class Books(Resource):
    @books_namespace.marshal_with(book)  # updated
    def get(self, book_id):
        book = get_book_by_id(book_id)
        if not book:
            books_namespace.abort(404, f"Name {book_id} does not exist")  # updated
        return book, 200

    @books_namespace.expect(book, validate=True)  # updated
    def put(self, book_id):
        post_data = request.get_json()
        title = post_data.get("title")
        author = post_data.get("author")
        response_object = {}

        book = get_book_by_id(book_id)
        if not book:
            books_namespace.abort(404, f"Name {book_id} does not exist")  # updated

        if get_book_by_title(title):
            response_object["message"] = "Sorry. That book already exists."
            return response_object, 400

        update_book(book, title, author)

        response_object["message"] = f"{book.id} was updated!"
        return response_object, 200

    def delete(self, book_id):
        response_object = {}
        book = get_book_by_id(book_id)

        if not book:
            books_namespace.abort(404, f"Name {book_id} does not exist")  # updated

        delete_book(book)

        response_object["message"] = f"{book.name} was removed!"
        return response_object, 200


books_namespace.add_resource(BooksList, "")  # updated
books_namespace.add_resource(Books, "/<int:book_id>")  # updated
