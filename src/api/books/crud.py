# src/api/crud.py

from src.api.models import Book
from src.extensions import db


def get_all_books():
    return Book.query.all()


def get_book_by_id(book_id):
    return Book.query.filter_by(id=book_id).first()


def get_book_by_title(title):
    return Book.query.filter_by(title=title).first()


def get_book_by_author(author):
    return Book.query.filter_by(author=author).first()


def add_book(title, author):
    book = Book(title=title, author=author)
    db.session.add(book)
    db.session.commit()
    return book


def update_book(book, title, author):
    book.title = title
    book.author = author
    db.session.commit()
    return book


def delete_book(book):
    db.session.delete(book)
    db.session.commit()
    return book
