# src/api/crud.py

from src.api.models import UserChannel
from src.extensions import db


def get_all_users_channels():
    return UserChannel.query.all()


def get_user_channel_by_id(user_channel_id):
    return UserChannel.query.filter_by(id=user_channel_id).first()


def get_user_channel_by_user_id(user_id):
    return UserChannel.query.filter_by(user_id=user_id).first()


def get_user_channel_by_channel_id(channel_id):
    return UserChannel.query.filter_by(channel_id=channel_id).first()


def get_user_and_channel(user_id, channel_id):
    return UserChannel.query.filter_by(user_id=user_id, channel_id=channel_id).first()


def add_user_channel(user_id, channel_id):
    user_channel = UserChannel(user_id=user_id, channel_id=channel_id)
    db.session.add(user_channel)
    db.session.commit()
    return user_channel


def update_user_channel(user_channel, user_id, channel_id):
    user_channel.user_id = user_id
    user_channel.channel_id = channel_id
    db.session.commit()
    return user_channel


def delete_user_channel(user_channel):
    db.session.delete(user_channel)
    db.session.commit()
    return user_channel
