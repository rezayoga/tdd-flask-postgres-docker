# src/api/users/views.py

from flask import request
from flask_restx import Namespace, Resource, fields  # updated

from src.api.users_channels.crud import (  # isort:skip
    get_all_users_channels,
    get_user_and_channel,
    # get_user_channel_by_channel_id,
    get_user_channel_by_id,
    # get_user_channel_by_user_id,
    add_user_channel,
    update_user_channel,
    delete_user_channel,
)

users_channels_namespace = Namespace("users_channels")  # new

# updated
user_channel = users_channels_namespace.model(
    "UserChannel",
    {
        "id": fields.Integer(readOnly=True),
        "user_id": fields.Integer(required=True),
        "channel_id": fields.Integer(required=True),
        "created_date": fields.DateTime,
    },
)


class UsersChannelsList(Resource):
    @users_channels_namespace.marshal_with(user_channel, as_list=True)  # updated
    def get(self):
        return get_all_users_channels(), 200

    @users_channels_namespace.expect(user_channel, validate=True)  # updated
    def post(self):
        post_data = request.get_json()
        user_id = post_data.get("user_id")
        channel_id = post_data.get("channel_id")
        response_object = {}

        user_channel = get_user_and_channel(user_id, channel_id)
        if user_channel:
            response_object["message"] = "Sorry. That user channel already exists."
            return response_object, 400

        add_user_channel(user_id, channel_id)

        response_object["message"] = f"{channel_id} was added!"
        return response_object, 201


class UsersChannels(Resource):
    @users_channels_namespace.marshal_with(user_channel)  # updated
    def get(self, user_channel_id):
        user_channel = get_user_channel_by_id(user_channel_id)
        if not user_channel:
            users_channels_namespace.abort(
                404, f"User {user_channel_id} does not exist"
            )  # updated
        return user_channel, 200

    @users_channels_namespace.expect(user_channel, validate=True)  # updated
    def put(self, user_channel_id):
        post_data = request.get_json()
        user_id = post_data.get("user_id")
        channel_id = post_data.get("channel_id")
        response_object = {}

        user_channel = get_user_channel_by_id(user_channel_id)
        if not user_channel:
            users_channels_namespace.abort(
                404, f"User {user_channel_id} does not exist"
            )  # updated

        if get_user_and_channel(user_id, channel_id):
            response_object["message"] = "Sorry. That channel_id already exists."
            return response_object, 400

        update_user_channel(user_channel, user_id, channel_id)

        response_object["message"] = f"{user_channel.id} was updated!"
        return response_object, 200

    def delete(self, user_channel_id):
        response_object = {}
        user_channel = get_user_channel_by_id(user_channel_id)

        if not user_channel:
            users_channels_namespace.abort(
                404, f"User {user_channel_id} does not exist"
            )  # updated

        delete_user_channel(user_channel)

        response_object["message"] = f"{user_channel.id} was removed!"
        return response_object, 200


users_channels_namespace.add_resource(UsersChannelsList, "")  # updated
users_channels_namespace.add_resource(
    UsersChannels, "/<int:user_channel_id>"
)  # updated
