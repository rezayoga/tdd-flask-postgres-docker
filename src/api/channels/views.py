# src/api/channels/views.py

from flask import request
from flask_restx import Namespace, Resource, fields  # updated

from src.api.channels.crud import (  # isort:skip
    get_all_channels,
    get_channel_by_name,
    add_channel,
    get_channel_by_id,
    update_channel,
    delete_channel,
)

channels_namespace = Namespace("channels")  # new

# updated
channel = channels_namespace.model(
    "Channel",
    {
        "id": fields.Integer(readOnly=True),
        "name": fields.String(required=True),
        "created_date": fields.DateTime,
    },
)


class ChannelsList(Resource):
    @channels_namespace.marshal_with(channel, as_list=True)  # updated
    def get(self):
        return get_all_channels(), 200

    @channels_namespace.expect(channel, validate=True)  # updated
    def post(self):
        post_data = request.get_json()
        name = post_data.get("name")
        response_object = {}

        channel = get_channel_by_name(name)
        if channel:
            response_object["message"] = "Sorry. That channel already exists."
            return response_object, 400

        add_channel(name)

        response_object["message"] = f"{name} was added!"
        return response_object, 201


class Channels(Resource):
    @channels_namespace.marshal_with(channel)  # updated
    def get(self, channel_id):
        channel = get_channel_by_id(channel_id)
        if not channel:
            channels_namespace.abort(
                404, f"Name {channel_id} does not exist"
            )  # updated
        return channel, 200

    @channels_namespace.expect(channel, validate=True)  # updated
    def put(self, channel_id):
        post_data = request.get_json()
        name = post_data.get("name")
        response_object = {}

        channel = get_channel_by_id(channel_id)
        if not channel:
            channels_namespace.abort(
                404, f"Name {channel_id} does not exist"
            )  # updated

        if get_channel_by_name(name):
            response_object["message"] = "Sorry. That channel already exists."
            return response_object, 400

        update_channel(channel, name)

        response_object["message"] = f"{channel.id} was updated!"
        return response_object, 200

    def delete(self, channel_id):
        response_object = {}
        channel = get_channel_by_id(channel_id)

        if not channel:
            channels_namespace.abort(
                404, f"Name {channel_id} does not exist"
            )  # updated

        delete_channel(channel)

        response_object["message"] = f"{channel.name} was removed!"
        return response_object, 200


channels_namespace.add_resource(ChannelsList, "")  # updated
channels_namespace.add_resource(Channels, "/<int:channel_id>")  # updated
