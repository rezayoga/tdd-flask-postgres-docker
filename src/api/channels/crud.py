# src/api/crud.py

from src.api.models import Channel
from src.extensions import db


def get_all_channels():
    return Channel.query.all()


def get_channel_by_id(channel_id):
    return Channel.query.filter_by(id=channel_id).first()


def get_channel_by_name(name):
    return Channel.query.filter_by(name=name).first()


def add_channel(name):
    channel = Channel(name=name)
    db.session.add(channel)
    db.session.commit()
    return channel


def update_channel(channel, name):
    channel.name = name
    db.session.commit()
    return channel


def delete_channel(channel):
    db.session.delete(channel)
    db.session.commit()
    return channel
