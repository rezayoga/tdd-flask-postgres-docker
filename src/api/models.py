# src/api/models.py

from sqlalchemy.sql import func
from werkzeug.security import check_password_hash, generate_password_hash

from src.extensions import db


class UserChannel(db.Model):
    __tablename__ = "users_channels"

    id = db.Column(db.Integer(), primary_key=True, autoincrement=True)
    user_id = db.Column(
        "user_id", db.Integer, db.ForeignKey("users.id"), primary_key=True
    )
    channel_id = db.Column(
        "channel_id", db.Integer, db.ForeignKey("channels.id"), primary_key=True
    )
    created_date = db.Column(db.DateTime, default=func.now(), nullable=False)

    # user = db.relationship("User", backref="users")
    # channel = db.relationship("Channel", backref="channels")

    def __repr__(self) -> str:
        return f"<Channel: {self.user_id} -  {self.channel_id}>"


class User(db.Model):

    __tablename__ = "users"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    public_id = db.Column(db.String(50), unique=True)
    username = db.Column(db.String(128), unique=True, nullable=False)
    email = db.Column(db.String(128), unique=True, nullable=False)
    password = db.Column(db.String(), nullable=False)
    active = db.Column(db.Boolean(), default=True, nullable=False)
    created_date = db.Column(db.DateTime, default=func.now(), nullable=False)
    following = db.relationship(
        "Channel",
        secondary="users_channels",
        backref=db.backref("followers", lazy="select"),
    )
    books = db.relationship("Book", backref="user")

    def __repr__(self) -> str:
        return f"{self.username} ({self.email})"

    def set_password(self, password) -> str:
        return generate_password_hash(password, method="sha256")

    def check_password(self, password):
        return check_password_hash(self.password, password)

    def __init__(self, public_id: str, username: str, email: str, password: str):
        self.public_id = public_id
        self.username = username
        self.email = email
        self.password = self.set_password(password)

    # Flask-Login integration
    # NOTE: is_authenticated, is_active, and is_anonymous
    # are methods in Flask-Login < 0.3.0
    @property
    def is_authenticated(self):
        return True

    @property
    def is_active(self):
        return True

    @property
    def is_anonymous(self):
        return False

    def get_id(self):
        return self.id

    # Required for administrative interface
    def __unicode__(self):
        return self.username


class Book(db.Model):

    __tablename__ = "books"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    title = db.Column(db.String(128), nullable=False)
    author = db.Column(db.String(128), nullable=False)
    year = db.Column(db.String(4), nullable=False)
    created_date = db.Column(db.DateTime, default=func.now(), nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey("users.id"))
    """ user = db.relationship('User',
                           backref=db.backref("books",
                                              uselist=True,
                                              lazy="select",
                                              cascade='delete-orphan,all')) """

    def __repr__(self) -> str:
        return f"{self.title} - {self.author} ({self.year})"


class Channel(db.Model):

    __tablename__ = "channels"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(128), nullable=False)
    created_date = db.Column(db.DateTime, default=func.now(), nullable=False)

    def __repr__(self) -> str:
        return f"{self.name}"

    def __init__(self, name):
        self.name = name
