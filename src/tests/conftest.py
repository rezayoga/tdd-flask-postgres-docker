# src/tests/conftest.py


import pytest

from src import create_app, db  # updated
from src.api.models import User


@pytest.fixture(scope="module")
def test_app():
    app = create_app()  # new
    app.config.from_object("src.config.TestingConfig")
    with app.app_context():
        yield app  # testing happens here


@pytest.fixture(scope="module")
def test_database():
    db.create_all()
    yield db  # testing happens here
    db.session.remove()
    db.drop_all()


@pytest.fixture(scope="function")
def add_user():
    def _add_user(public_id, username, email, password):
        user = User(
            public_id=public_id, username=username, email=email, password=password
        )
        db.session.add(user)
        db.session.commit()
        return user

    return _add_user
