# manage.py

import sys

from flask.cli import FlaskGroup

from src import create_app, db
from src.api.models import Channel  # new
from src.api.models import User
from src.api.models import Book
import uuid

app = create_app()  # new
cli = FlaskGroup(create_app=create_app)  # new


@cli.command('recreate_db')
def recreate_db():
    db.drop_all()
    db.create_all()
    db.session.commit()


@cli.command('seed_db')
def seed_db():
    u1 = User(public_id=str(uuid.uuid4()),
              username="reza",
              email="reza.yoga@gmail.com",
              password="reza")
    u2 = User(public_id=str(uuid.uuid4()),
              username="ainun",
              email="ainun.habibah@gmail.com",
              password="ainun")
    u3 = User(
        public_id=str(uuid.uuid4()),  # new
        username="musa",
        email="musa@gmail.com",
        password="musa")

    b1 = Book(title='book1', author='author1', user=u1, year='2020')
    b2 = Book(title='book2', author='author2', user=u2, year='2022')
    b3 = Book(title='book3', author='author3', user=u1, year='2021')
    b4 = Book(title='book4', author='author4', user=u2, year='2021')
    b5 = Book(title='book5', author='author5', user=u1, year='2019')
    b6 = Book(title='book6', author='author6', user=u2, year='2020')
    b7 = Book(title='book7', author='author7', user=u1, year='2021')
    b8 = Book(title='book8', author='author8', user=u2, year='2021')
    b9 = Book(title='book9', author='author9', user=u1, year='2022')
    b10 = Book(title='book10', author='author10', user=u3, year='2022')
    b11 = Book(title='book11', author='author11', user=u3, year='2021')
    b12 = Book(title='book12', author='author12', user=u3, year='2020')
    b13 = Book(title='book13', author='author13', user=u3, year='2019')
    b14 = Book(title='book14', author='author14', user=u1, year='2020')
    b15 = Book(title='book15', author='author15', user=u3, year='2021')
    b16 = Book(title='book16', author='author16', user=u1, year='2022')
    b17 = Book(title='book17', author='author17', user=u3, year='2023')
    b18 = Book(title='book18', author='author18', user=u2, year='2024')
    b19 = Book(title='book19', author='author19', user=u3, year='2025')
    b20 = Book(title='book20', author='author20', user=u1, year='2026')

    c1 = Channel(name='redis')
    c2 = Channel(name='python')
    c3 = Channel(name='flask')
    c4 = Channel(name='sqlalchemy')
    c5 = Channel(name='django')
    c6 = Channel(name='flask-restx')
    c7 = Channel(name='flask-mongoengine')
    c8 = Channel(name='flask-sqlalchemy')
    c9 = Channel(name='flask-marshmallow')
    c10 = Channel(name='flask-jwt-extended')
    c11 = Channel(name='flask-cors')
    c12 = Channel(name='flask-restplus')
    c13 = Channel(name='flask-jwt')
    c14 = Channel(name='flask-login')

    db.session.add_all([
        u1, u2, u3, b1, b2, b3, b4, b5, b6, b7, b8, b9, b10, b11, b12, b13,
        b14, b15, b16, b17, b18, b19, b20, c1, c2, c3, c4, c5, c6, c7, c8, c9,
        c10, c11, c12, c13, c14
    ])
    db.session.commit()

    u1.following.append(c1)
    u1.following.append(c4)
    u1.following.append(c7)
    u1.following.append(c6)
    u2.following.append(c2)
    u2.following.append(c3)
    u2.following.append(c5)
    u2.following.append(c6)
    u3.following.append(c1)
    u3.following.append(c4)
    u2.following.append(c7)
    u1.following.append(c14)

    db.session.commit()
    """ It is not immutable, but it is a bit special. You've configured your relationship and its backref using lazy='dynamic'. 
    This makes it so that in place of an instrumented collection you have a Query object – handy for handling large collections of related objects. 
    This query object has a few added methods and features in addition to the basic filter(), all() etc. of a query, such as append(), remove(), and support for assignment, 
    but basically it's a shortcut for querying related objects. Reading the docs further we find:
 """

    #print(type(u1.following))
    #print(type(c6.followers))

    #print(u1.following)
    #print(c6.followers)

    for following in u1.following:
        print(following.name)


if __name__ == '__main__':
    cli()