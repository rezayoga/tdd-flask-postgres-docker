"""empty message

Revision ID: 6fc3a8434289
Revises: 91facebce7d1
Create Date: 2022-07-29 06:36:38.468012

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '6fc3a8434289'
down_revision = '91facebce7d1'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('users', sa.Column('public_id', sa.String(length=50), nullable=True))
    op.create_unique_constraint(None, 'users', ['public_id'])
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint(None, 'users', type_='unique')
    op.drop_column('users', 'public_id')
    # ### end Alembic commands ###
